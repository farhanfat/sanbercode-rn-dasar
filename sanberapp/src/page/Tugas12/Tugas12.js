import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from '../../components/Tugas12/videoItem'
import data from '../../components/Tugas12/data.json';
import App from '../../../App';

export default class Tugas12 extends Component {
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.navBar}>
          <TouchableOpacity>
            <Image source={require('../../../assets/logo.png')} style ={{marginTop:40, marginLeft:10, width:98,height:22}} ></Image>
          </TouchableOpacity>
          <View style={styles.rightNav}>
            <TouchableOpacity>
              <Icon name="language-javascript" size={'25'} style ={{marginTop:45, marginRight:10}}/>
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon name="account-circle" size={'25'} style ={{marginTop:45, marginRight:20}}/>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.body}>
          {/* //<VideoItem video={data.items[0]}/> */}
          <FlatList 
            data={data.items}
            renderItem={(video)=><VideoItem video={video.item}  />}
            keyExtractor={(item)=>item.id}
            ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#e5e5e5'}}/>}
          />
        </View>
        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name ="home" size={25}/>
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name ="whatshot" size={25}/>
            <Text style={styles.tabTitle}>Trending</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name ="subscriptions" size={25}/>
            <Text style={styles.tabTitle}>Subscribe</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
            <Icon name ="folder" size={25}/>
            <Text style={styles.tabTitle}>Account</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navBar : {
    height : 95,
    backgroundColor : 'white',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    flexDirection : 'row',
    alignItems :'center',
    justifyContent :'space-between'
  },
  rightNav :{
    flexDirection:'row'
  },
  body :{
    flex:1
  },
  tabBar :{
    flexDirection:'row',
    backgroundColor:'white',
    borderTopWidth:0.5,
    borderColor:'#E5E5E5',
    height : 60,
    shadowRadius: 2,
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowColor: '#000000',
    elevation: 4,
    justifyContent:'space-around'
  },
  tabItem:{
    alignItems:'center',
    justifyContent:'center'
  },
  tabTitle:{
    fontSize:11,
    paddingTop:4,
    color:'#3c3c3c'
  }
});

