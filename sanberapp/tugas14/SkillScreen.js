import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity, FlatList, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Logo from './assets/logo.png'
import Gambar from './assets/FarhanPNG3.png'
import LogoReact from './assets/logo-react.png'
import Arrow from './assets/Vector.png'
import Skill from './components/Skill'
import data from './skillData.json'
import { ScrollView } from 'react-native-gesture-handler';


export default class AboutScreen extends Component {
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.headingIos}/>
        <Image source={Logo} style={{flexDirection:'row', borderColor:'grey',marginLeft:250, width: 150, height: 60, resizeMode: 'contain' }} />
        <View style={{flexDirection:'row'}}> 
          <Image source={Gambar} style={{marginLeft :10, justifyContent:'center',alignItems:'center',borderColor:'grey',borderWidth:1,borderRadius:130/2,height:50,width:50,flexDirection:'row'}} />
          <View>
            <Text style={{marginLeft :10,fontSize:18,color:'#00000'}}>Hai</Text>    
            <Text style={{marginLeft :10,fontSize:18,color:'#003366'}}>Farhan Faturohman</Text>    
          </View>
        </View>
        <Text style={{marginLeft :10, marginTop :20,fontSize:30,color:'#003366'}}>SKILL</Text>    
        <View style={styles.garis}/>
        <View style={{flexDirection:'row'}}>
          <View style={styles.kotakJudul}>
              <Text style={styles.textKotakJudul}>Library/Framework</Text>
          </View>
          <View style={styles.kotakJudul}>
              <Text style={styles.textKotakJudul}>Bahasa Pemograman</Text>
          </View>
          <View style={styles.kotakJudul}>
              <Text style={styles.textKotakJudul}>Teknologi</Text>
          </View>
        </View>
          <FlatList
            data={data.items}
            renderItem={(library)=><Skill library={library.item}  />}
            keyExtractor={(item)=>item.id}
          />
        
      </View>
    );
  };
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    headingIos: {
        height: 40
    },
    textInput: {
        marginTop:2,
        marginLeft:20,
        marginRight:20,
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1
    },
    garis :{
        flexDirection:'row',
        backgroundColor:'#B4E9FF',
        borderTopWidth:5.5,
        borderColor:'#B4E9FF',
    },
    kotakJudul :{
        backgroundColor:'#B4E9FF',
        borderRadius:10,
        marginLeft:10,
        marginTop:10
    },
    textKotakJudul :{
        color:'#003366',
        padding:7
    },
    kotakIsi :{
        
        backgroundColor:'#B4E9FF',
        borderRadius:10,
        marginLeft:10,
        marginRight:10,
        marginTop:10
    }

})
