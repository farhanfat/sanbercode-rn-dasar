import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LogoReact from '../assets/logo-react.png'
import Arrow from '../assets/Vector.png'


export default class Skill extends Component {
    render(){
        let library=this.props.library;
      return (
        <View style={styles.kotakIsi}>
            <View style={{flexDirection:'row'}}>
                <Icon name={library.iconName} size={'75'} style ={{marginTop:22, marginLeft:10}}/>
                <View style={{marginLeft :20}}>
                    <Text style={{fontSize :24, marginTop:5, marginLeft:10, fontWeight:'bold', color:'#003366'}}>{library.skillName}</Text>
                    <Text style={{fontSize :16, marginTop:5, marginLeft:10, fontWeight:'bold', color:'#3EC6FF'}}>{library.categoryName}</Text>
                    <Text style={{fontSize :48, marginTop:5, marginLeft:50, fontWeight:'bold', color:'white'}}>{library.percentageProgress}</Text>
                </View>
            <Image source={Arrow} style={{marginLeft :70, marginTop:30}} />
            </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    headingIos: {
        height: 40
    },
    textInput: {
        marginTop:2,
        marginLeft:20,
        marginRight:20,
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1
    },
    garis :{
        flexDirection:'row',
        backgroundColor:'#B4E9FF',
        borderTopWidth:5.5,
        borderColor:'#B4E9FF',
    },
    kotakJudul :{
        backgroundColor:'#B4E9FF',
        borderRadius:10,
        marginLeft:10,
        marginTop:10
    },
    textKotakJudul :{
        color:'#003366',
        padding:7
    },
    kotakIsi :{
        
        backgroundColor:'#B4E9FF',
        borderRadius:10,
        marginLeft:10,
        marginRight:10,
        marginTop:10
    }

})
