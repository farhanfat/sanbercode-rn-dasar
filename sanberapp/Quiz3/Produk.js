import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class Produk extends Component {
    render(){
        let produk=this.props.produk;
      return (
        <View style={{flex : 1, flexDirection:'row'}}>
            <View style={styles.kotakIsi}>
                <Image source={{uri: produk.gambaruri}} style={{width:80, height:50}}/>
                <Text style={{fontSize :12, marginTop:5, marginLeft:10, fontWeight:'bold', color:'#003366', alignContent : 'center', minWidth:170 ,maxWidth:170}}>{produk.nama}</Text>
                <Text style={{fontSize :12, marginTop:5, marginLeft:10, fontWeight:'bold', color:'#003366', alignContent : 'center'}}>Rp {produk.harga}</Text>
                <Text style={{fontSize :12, marginTop:5, marginLeft:10, fontWeight:'bold', color:'#003366', alignContent : 'center'}}>Sisa Stok {produk.stock}</Text>
                <TouchableOpacity>
                    <View style={{backgroundColor :'blue',marginTop:5,padding:5,marginBottom:5}}>
                        <Text style={{color :'white'}}>Beli</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.kotakIsi}>
                <Image source={{uri: produk.gambaruri}} style={{width:80, height:50}}/>
                <Text style={{fontSize :12, marginTop:5, marginLeft:10, fontWeight:'bold', color:'#003366', alignContent : 'center', minWidth:170 ,maxWidth:170}}>{produk.nama}</Text>
                <Text style={{fontSize :12, marginTop:5, marginLeft:10, fontWeight:'bold', color:'#003366', alignContent : 'center'}}>Rp {produk.harga}</Text>
                <Text style={{fontSize :12, marginTop:5, marginLeft:10, fontWeight:'bold', color:'#003366', alignContent : 'center'}}>Sisa Stok {produk.stock}</Text>
                <TouchableOpacity>
                    <View style={{backgroundColor :'blue',marginTop:5,padding:5,marginBottom:5}}>
                        <Text style={{color :'white'}}>Beli</Text>
                    </View>
                </TouchableOpacity>
                
            </View>
        </View>
        
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    headingIos: {
        height: 40
    },
    textInput: {
        marginTop:2,
        marginLeft:20,
        marginRight:20,
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1
    },
    garis :{
        flexDirection:'row',
        backgroundColor:'#B4E9FF',
        borderTopWidth:5.5,
        borderColor:'#B4E9FF',
    },
    kotakJudul :{
        backgroundColor:'white',
        borderRadius:10,
        marginLeft:10,
        marginTop:10
    },
    textKotakJudul :{
        color:'#003366',
        padding:7
    },
    kotakIsi :{
        maxWidth:300,
        backgroundColor:'white',
        borderRadius:10,
        marginLeft:10,
        marginRight:10,
        marginTop:10,
        alignContent : 'center',
        alignItems:'center', 
        justifyContent:'center',
        
    }

})
