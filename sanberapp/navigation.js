import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet, Text, View } from "react-native";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Login from './tugas13/src/LoginScreen'
import About from './tugas13/src/AboutScreen';
import Register from './tugas13/src/Register';
import Skill from './tugas14/SkillScreen';


const Drawer = createDrawerNavigator();

// const LoginStack = createStackNavigator();
// const LoginStackScreen = () => (
//     <LoginStack.Navigator>
//       <LoginStack.Screen name="Login" component={Login}/>
//     </LoginStack.Navigator>
// )


// const AboutStack = createStackNavigator();
// const AboutStackScreen = () => (
//     <AboutStack.Navigator>
//       <AboutStack.Screen name="About" component={About}/>
//     </AboutStack.Navigator>
// )
    
export default () => (
    <NavigationContainer>
        <Drawer.Navigator>
            <Drawer.Screen name='Login' component = {Login}/>
            <Drawer.Screen name='About' component = {About}/>
            <Drawer.Screen name='Register' component = {Register}/>
            <Drawer.Screen name='Skill' component = {Skill}/>
            
        </Drawer.Navigator>
    </NavigationContainer>
)