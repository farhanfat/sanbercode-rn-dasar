import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity, FlatList, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Logo from '../assets/logo.png'


export default class Register extends Component {
  render(){
    return (
      <View style={styles.container}>
          <View style={styles.headingIos}/>
          <View>
              <Image source={Logo} style={{justifyContent:'center',alignItems:'center'}} />
              <Text style={{marginTop:40,textAlign:'center',fontSize:24}}>Register</Text>
              <Text style={{marginTop:40,marginLeft:20,fontSize:16}}>Username</Text>
              <TextInput style={styles.textInput}/>
              <Text style={{marginTop:20,marginLeft:20,fontSize:16}}>Email</Text>
              <TextInput style={styles.textInput}/>
              <Text style={{marginTop:20,marginLeft:20,fontSize:16}}>Password</Text>
              <TextInput style={styles.textInput}/>
              <Text style={{marginTop:20,marginLeft:20,fontSize:16}}>Ulangi Password</Text>
              <TextInput style={styles.textInput}/>
              <View style = {{backgroundColor:'#003366', marginTop:40, marginLeft:150, marginRight:150, padding:10, borderRadius:50}}>
                <Text style ={{fontSize: 17,color:'white', textAlign:'center', fontWeight: 'bold',}}>Daftar</Text>
              </View>
              <Text style ={{fontSize: 17,color:'#3EC6FF', marginTop:10, textAlign:'center', fontWeight: 'bold',}}>Atau</Text>
              <View style = {{backgroundColor:'#3EC6FF', marginTop:10, marginLeft:150, marginRight:150, padding:10, borderRadius:50}}>
                <Text style ={{fontSize: 17,color:'white', textAlign:'center', fontWeight: 'bold',}}>Masuk ?</Text>
              </View>  
                
          </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    headingIos: {
        height: 80
    },
    textInput: {
        marginTop:2,
        marginLeft:20,
        marginRight:20,
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1
    }
})
