import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity, FlatList, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Logo from '../assets/logo.png'
import { createStackNavigator } from '@react-navigation/stack';
import navigation from '../../navigation';
import Login from '../../tugas13/src/LoginScreen'
import About from '../../tugas13/src/AboutScreen';
import Register from '../../tugas13/src/Register';
import Skill from '../../tugas14/SkillScreen';



export default class LoginScreen extends Component {
  render(){
    return (
      <View style={styles.container}>
          <View style={styles.headingIos}/>
          <View>
              <Image source={Logo} style={{justifyContent:'center',alignItems:'center'}} />
              <Text style={{marginTop:40,textAlign:'center', fontSize:24}}>Login</Text>
              <Text style={{marginTop:40,marginLeft:20,fontSize:16}}>Username/Email</Text>
              <TextInput style={styles.textInput}/>
              <Text style={{marginTop:20,marginLeft:20,fontSize:16}}>Password</Text>
              <TextInput style={styles.textInput}/>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Skill')}>
                <View style = {{backgroundColor:'#3EC6FF', marginTop:40, marginLeft:150, marginRight:150, padding:10, borderRadius:50}}>
                  <Text style ={{fontSize: 17,color:'white', textAlign:'center', fontWeight: 'bold',}}>Masuk</Text>
                </View>
              </TouchableOpacity>
              
              <Text style ={{fontSize: 17,color:'#3EC6FF', marginTop:10, textAlign:'center', fontWeight: 'bold',}}>Atau</Text>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                <View style = {{backgroundColor:'#003366', marginTop:10, marginLeft:150, marginRight:150, padding:10, borderRadius:50}}>
                  <Text style ={{fontSize: 17,color:'white', textAlign:'center', fontWeight: 'bold',}}>Daftar</Text>
                </View>  
              </TouchableOpacity>
          </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    headingIos: {
        height: 80
    },
    textInput: {
        marginTop:2,
        marginLeft:20,
        marginRight:20,
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1
    }
})
