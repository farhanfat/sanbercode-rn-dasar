import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Image, TouchableOpacity, FlatList, TextInput, Button } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Logo from '../assets/logo.png'
import Gambar from '../assets/FarhanPNG3.png'


export default class AboutScreen extends Component {
  render(){
    return (
      <View style={styles.container}>
          <View style={styles.headingIos}/>
          <View>
              <Text style={{marginTop:40,textAlign:'center',fontSize:24}}>Tentang Saya</Text>
              <Image source={Gambar} style={{marginLeft:140,marginTop:10,justifyContent:'center',alignItems:'center',borderColor:'grey',borderWidth:1,borderRadius:130/2,height:130,width:130,flexDirection:'row'}} />
              <Text style={{marginTop:20,textAlign:'center',fontSize:22,color:'#003366'}}>Farhan Faturohman</Text>
              <Text style={{textAlign:'center',fontSize:20,color:'#3EC6FF'}}>React Native Developer</Text>
              <View style={{borderColor:'grey',borderWidth:1, margin:10, backgroundColor:'#EFEFEF',borderRadius:10}}>
                <Text style={{fontSize:18,marginLeft:10}}>Portofolio</Text>
                <View style={styles.garis}/>
                <View style={{display : 'flex', flexDirection:'row', justifyContent: 'space-around'}}>
                  <View style={{alignItems:'center'}}>
                    <Icon name={'gitlab'} size={'55'} style ={{color:'#3EC6FF'}}/>
                    <Text>@farhanfat</Text>
                  </View>
                  <View style={{alignItems:'center'}}>
                    <Icon name={'git'} size={'55'} style ={{color:'#3EC6FF'}}/>
                    <Text>@farhanfat</Text>
                  </View>
                </View>
              </View>
              <View style={{borderColor:'grey',borderWidth:1, margin:10, backgroundColor:'#EFEFEF',borderRadius:10}}>
                <Text style={{fontSize:18,marginLeft:10}}>Hubungi Saya</Text>
                <View style={styles.garis}/>      
                  <View style={{alignItems:'center'}}>
                    <Icon name={'facebook'} size={'55'} style ={{color:'#3EC6FF'}}/>
                    <Text>@farhanfat</Text>
                  </View>
                  <View style={{alignItems:'center'}}>
                    <Icon name={'instagram'} size={'55'} style ={{color:'#3EC6FF'}}/>
                    <Text>@farhanfat</Text>
                  </View>
                  <View style={{alignItems:'center'}}>
                    <Icon name={'twitter'} size={'55'} style ={{color:'#3EC6FF'}}/>
                    <Text>@farhanfat</Text>
                  </View>
              </View>
          </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    headingIos: {
        height: 80
    },
    garis :{
      flexDirection:'row',
      borderWidth:2,
      borderColor:'#000000',
      margin:5
    },
    textInput: {
        marginTop:2,
        marginLeft:20,
        marginRight:20,
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1
    }
})
